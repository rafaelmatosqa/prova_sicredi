
#Desafio Automação de Testes - API.

## Objetivo do projeto.

Esse projeto de automação de testes foi criado com o objetivo de atender ao desafio de API do Sicredi.

##Tecnologias utilizadas no projeto.
* RestAssured
* Linguagem Java
* JUnit 5
* Allure Report

##Padrão de projeto utilizado.
* Test Data Factory para gerar os dados fake e Builder para criar o objeto dos testes.
* Data Provider + arguments do JUNIT 5 para economizar linhas de codigo
e poder executar varios cenários de falha em um unico teste.

## Requisitos de software.
* Java 8+ JDK deve estar instalado.
* Maven instalado e configurado no path da aplicação.
* Você realizar o build do projeto backend enviado em anexo para ter acesso a documentação das API'S que serão automatizadas.

## Como executar os testes e gerar o report.

* Os testes foram divididos em teste de contrato e testes funcionais:
* Todos os testes : mvn clean test
* Testes de contrato: mvn clean test -Dgroups="contrato" 
* Testes de funcionais: mvn clean test -Dgroups="funcional"
* Gerar o report: mvn allure:serve

## Cenários de teste realizados.
![img_2.png](img_2.png)

![img_3.png](img_3.png)

##Resultado dos testes executados.

![img_1.png](img_1.png)

* Foram identificados 7 bugs no backend que de acordo com os requisitos o mesmo não cumpriu.