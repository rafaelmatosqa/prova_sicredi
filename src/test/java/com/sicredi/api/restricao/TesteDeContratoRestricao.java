package com.sicredi.api.restricao;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import java.util.Arrays;
import java.util.List;
import static com.sicredi.api.utils.TagsExec.CONTRATO;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

class TesteDeContratoRestricao extends BaseRestricao {

    @Test
    @Tag(CONTRATO)
    @DisplayName("Validar o contrato de retorno de todos os cpf's com restricao")
    void contrato() {

        List<String> cpfComRestricao =
                Arrays.asList("97093236014", "60094146012", "84809766080",
                        "62648716050", "26276298085", "01317496094", "55856777050",
                        "19626829001", "24094592008", "58063164083");
        int i;
        int n = cpfComRestricao.size();

        for (i = 0; i < n; i++) {
            given().
                    pathParam("cpf", cpfComRestricao.get(i)).
                    when().
                    get("/restricoes/{cpf}").
                    then().
                    body(matchesJsonSchemaInClasspath("contratos/contrato_2xx_restricao.json"));


        }

    }
}
