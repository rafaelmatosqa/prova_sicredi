package com.sicredi.api.restricao;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;

import static com.sicredi.api.utils.TagsExec.FUNCIONAL;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

public class TesteFuncionalRestricao extends BaseRestricao {


    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar o retorno do teste quando o cpf é sem restrição")
    void cpfSemRestricao() {
        given().
                pathParam("cpf", factoryRestricao.cpfSemRestricao()).
                when().
                get("/restricoes/{cpf}").
                then()
                .statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar a mensagem de resposta para cada consulta de cpf com restrição")
    void cpfComRestricao() {

        List<String> cpfComRestricao =
                Arrays.asList("97093236014", "60094146012", "84809766080",
                        "62648716050", "26276298085", "01317496094", "55856777050",
                        "19626829001", "24094592008", "58063164083");
        int i;
        int n = cpfComRestricao.size();

        for (i = 0; i < n; i++) {

            given().
                    pathParam("cpf", cpfComRestricao.get(i)).
                    when().
                    get("/restricoes/{cpf}").
                    then().assertThat()
                    .statusCode(HttpStatus.SC_OK)
                    .body("mensagem",
                            is(MessageFormat.format("O CPF {0} tem problema", cpfComRestricao.get(i))));

        }
    }
}
