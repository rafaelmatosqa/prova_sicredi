package com.sicredi.api.simulacao;


import com.sicredi.api.pojo.Simulacao;
import io.restassured.http.ContentType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.MessageFormat;

import static com.sicredi.api.utils.TagsExec.FUNCIONAL;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.apache.http.HttpStatus.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class TesteFuncionalSimulacao extends BaseSimulacao {

    private static final String CENARIOS_DE_FALHA = "com.sicredi.api.data.dataProvider.dataProviderSimulacao#validacaoDasPossiveisFalhasNaCriacaoDeUmaSimulacao";

    /**
     * Implementando cenários de falha da criação de simulacao
     */
    @Tag(FUNCIONAL)
    @ParameterizedTest(name = "Scenario: {2}")
    @MethodSource(value = CENARIOS_DE_FALHA)
    @DisplayName("Deve validar todos os cenarios de falha")
    void validarCenariosDeFalhaNaCriacaoDeSimulacoes(Simulacao simulacaoDeFalhas, String resposta_path, String mensagemValidada) {
        given().
                contentType(ContentType.JSON).
                body(simulacaoDeFalhas).
                when().
                post("/simulacoes").
                then().
                statusCode(SC_BAD_REQUEST).
                body(resposta_path, is(mensagemValidada));
    }

    /**
     * Implementando cenários da feature de get simulação
     */
    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar todas as simulacoes cadastradas")
    void buscarTodasAsSimulacoesCadastradas() {

        Simulacao[] listaSimulacõesCadastradas = factorySimulacao.todasSimulacoes();

        Simulacao[] req_simulacao =
                when().
                        get("/simulacoes").
                        then().
                        statusCode(SC_OK).
                        extract().
                        as(Simulacao[].class);

        Assertions.assertThat(listaSimulacõesCadastradas).contains(req_simulacao);
    }


    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar uma simulacao específica")
    void buscarUmaSimulacaoEspecifica() {
        Simulacao simulacaoEspecifica = factorySimulacao.simulacaoExistente();

        given().
                pathParam("cpf", simulacaoEspecifica.getCpf()).
                when().
                get("/simulacoes/{cpf}").
                then().
                statusCode(SC_OK).
                body(
                        "nome", equalTo(simulacaoEspecifica.getNome()),
                        "cpf", equalTo(simulacaoEspecifica.getCpf()),
                        "email", equalTo(simulacaoEspecifica.getEmail()),
                        "valor", equalTo(simulacaoEspecifica.getValor()),
                        "parcelas", equalTo(simulacaoEspecifica.getParcelas()),
                        "seguro", equalTo(simulacaoEspecifica.getSeguro())
                );
    }

    @Test
    @Tag("FUNCIONAL")
    @DisplayName("Deve retornar mensagem de erro informado que o cpf não foi encontrado")
    void SimulacaoNaoEncontrada() {
        String cpf =  factorySimulacao.cpfInexistente();
        given().
                pathParam("cpf", cpf).
                when().
                get("/simulacoes/{cpf}").
                then().
                statusCode(SC_NOT_FOUND).
                body("mensagem", is(MessageFormat.format("CPF {0} não encontrado", cpf)));
    }

    /**
     * Implementando cenários da feature de criar simulação
     */
    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve criar uma simulação nova e validar o resultado")
    void criarNovaSimulacao() {
        Simulacao s_valida = factorySimulacao.simulacaoValida();

        Simulacao response =
                given().
                        contentType(ContentType.JSON).
                        body(s_valida).
                        when().
                        post("/simulacoes").
                        then().
                        statusCode(SC_CREATED).
                        extract().
                        as(Simulacao.class);
        assertThat("A simulacao é a mesma enviada na criação", response, is(s_valida));
    }

    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar cadastro de simulacao com cpf duplicado")
    void simulacaoCPFDuplicado() {
        Simulacao cpfExistente = factorySimulacao.simulacaoExistente();
        given().
                contentType(ContentType.JSON).
                body(cpfExistente).
                when().
                post("/simulacoes/").
                then().
                statusCode(SC_CONFLICT).
                body("mensagem", is("CPF já existente"));
    }

    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve atualizar o valor de uma simulcao existente")
    void atualizarValorDeUmaSimulacaoCadastrada() {
        Simulacao simulacaoExistente = factorySimulacao.simulacaoExistente();

        Simulacao simulacao = factorySimulacao.simulacaoValida();
        simulacao.setValor(simulacaoExistente.getValor());
        simulacao.setCpf(simulacaoExistente.getCpf());

        Simulacao response =
                given().
                        contentType(ContentType.JSON).
                        pathParam("cpf", simulacaoExistente.getCpf()).
                        body(simulacao).
                        when().
                            put("/simulacoes/{cpf}").
                        then().
                             statusCode(SC_OK).
                        extract().
                            as(Simulacao.class);

        assertThat("Simulação foi atualizada",
                response, is(simulacao));
    }

    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve validar mensagem de retorno quando tentamos atualizar simulação com cpf não cadastrado")
    void atualizarSimulacaoComCPFnAOcADASTRADO() {
        Simulacao simulacao = factorySimulacao.simulacaoValida();
        String cpfInexistente = factorySimulacao.cpfInexistente();
        given().
                contentType(ContentType.JSON).
                pathParam("cpf", cpfInexistente).
                body(simulacao).
                when().
                put("/simulacoes/{cpf}").
                then().
                statusCode(SC_NOT_FOUND).
                body("mensagem", is(MessageFormat.format("CPF {0} não encontrado", cpfInexistente)));
    }

    /**
     * Implementando cenários da feature de delete simulação
     */
    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve deletar uma simulação existente")
    void apagarSimulacaoComSucesso() {
        Simulacao simulacao_existente = factorySimulacao.simulacaoExistente();

        given().
                pathParam("cpf", simulacao_existente.getCpf()).
                when().
                delete("/simulacoes/{cpf}").
                then().
                statusCode(SC_NO_CONTENT);
    }



    @Test
    @Tag(FUNCIONAL)
    @DisplayName("Deve deletar uma simulação com ID inexistente")
    void apagarSimulacaoComIDInexistente() {
        String id_inexistente = factorySimulacao.cpfInexistente();

        given().
                pathParam("id", id_inexistente).
                when().
                delete("/simulacoes/{id}").
                then().
                statusCode(SC_NOT_FOUND);
    }

}



