package com.sicredi.api.simulacao;

import com.sicredi.api.Base;
import com.sicredi.api.data.dataFactory.FactorySimulacao;
import org.junit.jupiter.api.BeforeAll;

public abstract class BaseSimulacao extends Base {

    protected static FactorySimulacao factorySimulacao;

    @BeforeAll
    static void setup() {
        factorySimulacao = new FactorySimulacao();
    }
}
