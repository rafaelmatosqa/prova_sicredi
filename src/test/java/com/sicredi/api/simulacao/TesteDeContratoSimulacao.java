package com.sicredi.api.simulacao;

import com.sicredi.api.Base;
import com.sicredi.api.data.dataFactory.FactorySimulacao;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class TesteDeContratoSimulacao extends Base {

    private static FactorySimulacao factorySimulacao;


    @BeforeAll
    static void setup() {
        factorySimulacao = new FactorySimulacao();
    }

    @Test
    @Tag("contrato")
    @DisplayName("Deve retornar o contrato de uma simulacao válida que foi criada")
    void simulacaoCriadaComSucesso() {
        given().
                contentType(ContentType.JSON).
                body(factorySimulacao.simulacaoValida()).
                when().
                post("/simulacoes").
                then().
                body(matchesJsonSchemaInClasspath("contratos/contrato_201_simulacao.json"));
    }


    @Test
    @Tag("contrato")
    @DisplayName("Deve validar o contrato de retorno quando é realizada uma pesquisa por uma simulacao específica")
    void getCpfJaCadastrado() {
        String cpfJaCadastrado = factorySimulacao.simulacaoExistente().getCpf();

        given().
                pathParam("cpf", cpfJaCadastrado).
                when().
                get("/simulacoes/{cpf}").
                then().
                body(matchesJsonSchemaInClasspath("contratos/contrato_simulacao_existente.json"));
    }

    @Test
    @Tag("contrato")
    @DisplayName("Deve validar o contrato de uma consulta de simulacao com cpf não cadastrado")
    void SimulacaoNaoEncontrada() {
        given().
                pathParam("cpf", factorySimulacao.cpfInexistente()).
                when().
                get("/simulacoes/{cpf}").
                then().
                body(matchesJsonSchemaInClasspath("contratos/contrato_cpf_inexistente.json"));
    }

    @Test
    @Tag("contrato")
    @DisplayName("Deve validar o contrato de consulta de todas as simulações")
    void todasAsSimulacoes() {
        given().

                when().
                get("/simulacoes").
                then().
                body(matchesJsonSchemaInClasspath("contratos/contrato_todas_simulacoes.json"));
    }


}

