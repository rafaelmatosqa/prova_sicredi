package com.sicredi.api;

import com.sicredi.api.config.ConfigManager;
import com.sicredi.api.config.Configuracao;
import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.path.json.config.JsonPathConfig;
import org.junit.jupiter.api.BeforeAll;

import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.config;
import static io.restassured.RestAssured.port;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.config.RestAssuredConfig.newConfig;

public abstract class Base {

    protected static Configuracao configuracao;

    @BeforeAll
    public static void AntesDeTodosOsTestes() {
        configuracao = ConfigManager.getConfiguration();
        baseURI = configuracao.baseURI();
        basePath = configuracao.basePath();
        port = configuracao.port();

        config = newConfig().
                jsonConfig(jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL)).
                sslConfig(new SSLConfig().allowAllHostnames());

        RestAssured.useRelaxedHTTPSValidation();


    }
}


