package com.sicredi.api.utils;

import java.util.Random;

public class GeradorDeCPF {


    public String gerarCPF() {

        Random r = new Random();
        StringBuilder numeroCPF = new StringBuilder();

        for (int i = 0; i < 9; i++) {

            numeroCPF.append(r.nextInt(9));

        }

        return gerarDigitos(numeroCPF.toString());

    }

    public boolean validarCPF(String cpf) {

        if (cpf.length() == 11) {

            if (cpf.equals(gerarDigitos(cpf.substring(0, 9)))) {

                return true;

            }

        }

        return false;

    }


    private String gerarDigitos(String digitosValidadores) {

        StringBuilder numeroCPF = new StringBuilder(digitosValidadores);

        int total = 0;
        int multiple = digitosValidadores.length() + 1;

        for (char digit : digitosValidadores.toCharArray()) {
            long parcial = Integer.parseInt(String.valueOf(digit)) * (multiple--);
            total += parcial;

        }

        int resto = Integer.parseInt(String.valueOf(Math.abs(total % 11)));

        if (resto < 2) {

            resto = 0;

        } else {

            resto = 11 - resto;

        }

        numeroCPF.append(resto);

        if (numeroCPF.length() < 11) {
            return gerarDigitos(numeroCPF.toString());

        }

        return numeroCPF.toString();

    }
}
