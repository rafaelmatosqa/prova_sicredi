
package com.sicredi.api.utils.commons;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.sicredi.api.config.ConfigManager;
import com.sicredi.api.config.Configuracao;

public class MessageFormat {
    private static final Logger log = LogManager.getLogger(MessageFormat.class);

    private MessageFormat() {
    }

    public static String locationURLByEnvironment() {
        String locationURL;
        Configuracao configuracao = ConfigManager.getConfiguration();

        locationURL = configuracao.port() < 8000 ? java.text.MessageFormat
                .format("{0}{1}/simulacoes/", configuracao.baseURI(), configuracao.basePath())
                : java.text.MessageFormat.format("{0}:{1}{2}/simulacoes/", configuracao.baseURI(),
                String.valueOf(configuracao.port()), configuracao.basePath());
        log.debug(locationURL);

        return locationURL;
    }
}
