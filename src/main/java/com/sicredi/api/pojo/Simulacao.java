package com.sicredi.api.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.math.BigDecimal;
import java.util.Objects;

public class Simulacao {

    @JsonIgnore
    private Long id;
    private String nome;
    private String cpf;
    private String email;
    private BigDecimal valor;
    private Integer parcelas;
    private Boolean seguro;

    public Simulacao(String nome, String cpf, String email, BigDecimal valor, Integer parcelas, Boolean seguro) {
        this.nome = nome;
        this.cpf = cpf;
        this.email = email;
        this.valor = valor;
        this.parcelas = parcelas;
        this.seguro = seguro;
    }
    public Simulacao() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getParcelas() {
        return parcelas;
    }

    public void setParcelas(Integer parcelas) {
        this.parcelas = parcelas;
    }

    public Boolean getSeguro() {
        return seguro;
    }

    public void setSeguro(Boolean seguro) {
        this.seguro = seguro;
    }

    @Override
    public String toString() {
        return "Simulacao{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", cpf=" + cpf +
                ", email='" + email + '\'' +
                ", valor=" + valor +
                ", parcelas=" + parcelas +
                ", seguro=" + seguro +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Simulacao s = (Simulacao) obj;
        return Objects.equals(id, s.id) &&
                Objects.equals(nome, s.nome) &&
                Objects.equals(cpf, s.cpf) &&
                Objects.equals(email, s.email) &&
                Objects.equals(valor, s.valor) &&
                Objects.equals(parcelas, s.parcelas) &&
                Objects.equals(seguro, s.seguro);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, cpf, email, valor, parcelas, seguro);
    }
}


