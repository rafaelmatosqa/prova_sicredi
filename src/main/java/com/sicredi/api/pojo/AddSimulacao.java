package com.sicredi.api.pojo;

import java.math.BigDecimal;

public class AddSimulacao {

    private String nome;
    private String cpf;
    private String email;
    private BigDecimal valor;
    private Integer parcelas;
    private Boolean seguro;


    public AddSimulacao nome(String nome) {
        this.nome = nome;
        return this;
    }

    public AddSimulacao cpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public AddSimulacao email(String email) {
        this.email = email;
        return this;
    }

    public AddSimulacao valor(BigDecimal valor) {
        this.valor = valor;
        return this;
    }

    public AddSimulacao parcelas(Integer parcelas) {
        this.parcelas = parcelas;
        return this;
    }

    public AddSimulacao seguro(Boolean seguro) {
        this.seguro = seguro;
        return this;
    }

    public Simulacao build() {
        return new Simulacao(nome, cpf, email, valor, parcelas, seguro);
    }


}



