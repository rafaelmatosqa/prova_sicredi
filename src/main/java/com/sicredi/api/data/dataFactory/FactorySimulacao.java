package com.sicredi.api.data.dataFactory;

import com.github.javafaker.Faker;
import com.sicredi.api.pojo.AddSimulacao;
import com.sicredi.api.pojo.Simulacao;
import com.sicredi.api.utils.GeradorDeCPF;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import java.math.BigDecimal;
import java.util.Random;
import static io.restassured.RestAssured.when;


public class FactorySimulacao {

    private static final int VALOR_MAXIMO = 40000;
    private static final int VALOR_MINIMO = 1000;
    private static final int PARCELA_MAXIMA = 48;
    private static final int PARCELA_MINIMA = 2;
    private final Faker faker;

    public FactorySimulacao() {
        faker = new Faker();
    }

    public Simulacao simulacaoValida() {
        return novaSimulacaoValida();
    }

    private Simulacao novaSimulacaoValida() {
        return new AddSimulacao().
                nome(faker.name().fullName()).
                cpf(new GeradorDeCPF().gerarCPF()).
                email(faker.internet().emailAddress()).
                valor(new BigDecimal(faker.number().numberBetween(VALOR_MINIMO, VALOR_MAXIMO))).
                parcelas(faker.number().numberBetween(PARCELA_MINIMA, PARCELA_MAXIMA)).
                seguro(faker.bool().bool()).build();
    }


    public Simulacao criarSimulacaoComValorMenorDoQueOValorMinimo() {
        Simulacao s = simulacaoValida();
        s.setValor(new BigDecimal(faker.number().numberBetween(1, VALOR_MINIMO - 1)));
        return s;
    }

    public Simulacao criarSimulacaoComValormaiorDoQueOValorMaximo() {
        Simulacao s = simulacaoValida();
        s.setValor(new BigDecimal(faker.number().numberBetween(VALOR_MAXIMO + 1, 99999)));
        return s;
    }

    public Simulacao criarSimulacaoComNumeroDeParcelasInferiorAoMinimo() {
        Simulacao s = simulacaoValida();
        s.setParcelas(PARCELA_MINIMA - 1);
        return s;
    }

    public Simulacao criarSimulacaoComNumeroDeParcelasSuperiorAoNumeroMaximo() {
        Simulacao s = simulacaoValida();
        s.setParcelas(PARCELA_MAXIMA + 1);
        return s;
    }

    public Simulacao simulacaoSemAtributoCPF() {
        return new AddSimulacao().
                nome(faker.name().fullName()).
                email(faker.internet().emailAddress()).
                valor(new BigDecimal(faker.number().numberBetween(VALOR_MINIMO, VALOR_MAXIMO))).
                parcelas(faker.number().numberBetween(PARCELA_MINIMA, PARCELA_MAXIMA)).
                seguro(faker.bool().bool()).build();
    }

    public Simulacao simulacaoSemAtributoNome() {
        return new AddSimulacao().
                cpf(new GeradorDeCPF().gerarCPF()).
                email(faker.internet().emailAddress()).
                valor(new BigDecimal(faker.number().numberBetween(VALOR_MINIMO, VALOR_MAXIMO))).
                parcelas(faker.number().numberBetween(PARCELA_MINIMA, PARCELA_MAXIMA)).
                seguro(faker.bool().bool()).build();
    }

    public Simulacao simulacaoSemAtributoEmail() {
        return new AddSimulacao().
                nome(faker.name().fullName()).
                cpf(new GeradorDeCPF().gerarCPF()).
                valor(new BigDecimal(faker.number().numberBetween(VALOR_MINIMO, VALOR_MAXIMO))).
                parcelas(faker.number().numberBetween(PARCELA_MINIMA, PARCELA_MAXIMA)).
                seguro(faker.bool().bool()).build();
    }

    public Simulacao simulacaoSemAtributoValor() {

        return new AddSimulacao().
                nome(faker.name().fullName()).
                cpf(new GeradorDeCPF().gerarCPF()).
                email(faker.internet().emailAddress()).
                parcelas(faker.number().numberBetween(PARCELA_MINIMA, PARCELA_MAXIMA)).
                seguro(faker.bool().bool()).build();

    }

    public Simulacao simulacaoSemAtributoParcelas() {
        return new AddSimulacao().
                nome(faker.name().fullName()).
                cpf(new GeradorDeCPF().gerarCPF()).
                email(faker.internet().emailAddress()).
                valor(new BigDecimal(faker.number().numberBetween(VALOR_MINIMO, VALOR_MAXIMO))).
                seguro(faker.bool().bool()).build();
    }


    public Simulacao simulacaoComEmailInvalido() {

        Simulacao s = simulacaoValida();
        s.setEmail(faker.name().username());
        return s;
    }

    public Simulacao simulacaoComTodosOsCamposInvalidos() {
        return new AddSimulacao().
                cpf(StringUtils.EMPTY).
                nome(StringUtils.EMPTY).
                email(faker.name().username()).
                valor(new BigDecimal(faker.number().numberBetween(1, VALOR_MINIMO - 1))).
                parcelas(PARCELA_MINIMA - 1).
                seguro(faker.bool().bool()).
                build();
    }

    public Simulacao simulacaoComCPFVazio() {
        Simulacao s = simulacaoValida();
        s.setCpf(StringUtils.EMPTY);

        return s;
    }

    public Simulacao simulacaoComNomeVazio() {
        Simulacao s = simulacaoValida();
        s.setNome(StringUtils.EMPTY);

        return s;
    }

    public Simulacao simulacaoComEmailVazio() {
        Simulacao s = simulacaoValida();
        s.setEmail(StringUtils.EMPTY);

        return s;
    }



    public Simulacao simulacaoExistente() {
        Simulacao[] sm = todasAsSimulacoesCadastradas();
        return sm[new Random().nextInt(sm.length)];
    }

    public Simulacao[] todasSimulacoes() {
        return todasAsSimulacoesCadastradas();
    }
    private Simulacao[] todasAsSimulacoesCadastradas() {
        return
                when().
                        get("/simulacoes/").
                        then().
                        statusCode(HttpStatus.SC_OK).
                        extract().
                        as(Simulacao[].class);
    }

    public String cpfInexistente() {
        return String.valueOf(new GeradorDeCPF().gerarCPF());

    }



}


