package com.sicredi.api.data.dataProvider;

import com.sicredi.api.data.dataFactory.FactorySimulacao;
import com.sicredi.api.pojo.Simulacao;
import static org.junit.jupiter.params.provider.Arguments.*;
import java.util.stream.Stream;
import org.junit.jupiter.params.provider.Arguments;

public class dataProviderSimulacao {

    public dataProviderSimulacao() {

    }

    public static Stream<Arguments> validacaoDasPossiveisFalhasNaCriacaoDeUmaSimulacao() {
        FactorySimulacao s = new FactorySimulacao();

        Simulacao simulacaoSemCPF = s.simulacaoSemAtributoCPF();
        Simulacao simulacaoSemNome = s.simulacaoSemAtributoNome();
        Simulacao simulacaoSemEmail = s.simulacaoSemAtributoEmail();
        Simulacao simulacaocomCPFVazio = s.simulacaoComCPFVazio();
        Simulacao simulacaoComNomeVazio = s.simulacaoComNomeVazio();
        Simulacao simulacaoComEmailVazio = s.simulacaoComEmailVazio();
        Simulacao simulacaoComEmailInvalido = s.simulacaoComEmailInvalido();
        Simulacao simulacaoSemValor = s.simulacaoSemAtributoValor();
        Simulacao simulacaoSemParcelas = s.simulacaoSemAtributoParcelas();
        Simulacao simulacaoValorMaiorQueOValorMaximo = s.criarSimulacaoComValormaiorDoQueOValorMaximo();
        Simulacao simulacaoMenorQueoValorMinimo = s.criarSimulacaoComValorMenorDoQueOValorMinimo();
        Simulacao simulacaoParcelasMenorQueOMinimo = s.criarSimulacaoComNumeroDeParcelasInferiorAoMinimo();
        Simulacao simulacaoParcelasMaiorQueOMaximo = s.criarSimulacaoComNumeroDeParcelasSuperiorAoNumeroMaximo();

        return Stream.of(
                arguments(simulacaoSemCPF, "erros.cpf", "CPF não pode ser vazio"),
                arguments(simulacaoSemNome, "erros.nome", "Nome não pode ser vazio"),
                arguments(simulacaoSemEmail, "erros.email", "E-mail não deve ser vazio"),
                arguments(simulacaoComEmailInvalido, "erros.email", "E-mail deve ser um e-mail válido"),
                arguments(simulacaoSemValor, "erros.valor", "Valor não pode ser vazio"),
                arguments(simulacaoSemParcelas, "erros.parcelas", "Parcelas não pode ser vazio"),
                arguments(simulacaoValorMaiorQueOValorMaximo, "erros.valor", "Valor deve ser menor ou igual a R$ 40.000"),
                arguments(simulacaoMenorQueoValorMinimo, "erros.valor", "Valor não deve ser menor que R$ 1.000"),
                arguments(simulacaoParcelasMaiorQueOMaximo, "erros.parcelas", "Parcelas deve ser menor ou igual a 48"),
                arguments(simulacaoParcelasMenorQueOMinimo, "erros.parcelas", "Parcelas deve ser igual ou maior que 2"),
                arguments(simulacaocomCPFVazio, "erros.cpf", "CPF não pode ser vazio"),
                arguments(simulacaoComNomeVazio, "erros.nome", "Nome não pode ser vazio"),
                arguments(simulacaoComEmailVazio, "erros.email", "E-mail deve ser um e-mail válido")


        );

    }


}
