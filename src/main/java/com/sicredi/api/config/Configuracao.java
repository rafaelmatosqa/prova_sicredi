package com.sicredi.api.config;

import org.aeonbits.owner.Config;


@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "classpath:config.properties"})
public interface Configuracao extends Config {

    @Key("config.base.path")
    String basePath();

    @Key("config.base.uri")
    String baseURI();

    @Key("config.port")
    int port();

    @Key("log.all")
    boolean logAll();


}








