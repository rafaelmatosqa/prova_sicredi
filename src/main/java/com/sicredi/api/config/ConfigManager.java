package com.sicredi.api.config;

import org.aeonbits.owner.ConfigCache;

public class ConfigManager {
    private ConfigManager() {
    }

    public static Configuracao getConfiguration() {
        return ConfigCache.getOrCreate(Configuracao.class);
    }


}
